package com.bv.commentproducerapp.controller;

import com.bv.commentproducerapp.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;

@CrossOrigin("*")
@RestController
@RequestMapping("comments")
public class CommentController {
    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping(value = "create", consumes = MediaType.TEXT_PLAIN)
    public void createAndPublish(@RequestBody String comment) {
        var created = commentService.persist(comment);
        commentService.publish(created);
    }
}
