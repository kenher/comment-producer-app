package com.bv.commentproducerapp.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "comments")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    @Column(nullable = false)
    public LocalDateTime created;
    @Column(columnDefinition = "TEXT NOT NULL")
    public String content;

    protected Comment() {}

    public Comment(String content) {
        this.content = content;
        this.created = LocalDateTime.now();
    }
}
