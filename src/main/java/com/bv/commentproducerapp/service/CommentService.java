package com.bv.commentproducerapp.service;

import com.bv.commentproducerapp.entity.BvTopics;
import com.bv.commentproducerapp.entity.Comment;
import com.bv.commentproducerapp.dao.CommentDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Slf4j
@Service
public class CommentService {
    private final KafkaTemplate<String, Comment> kafkaTemplate;
    private final CommentDao commentDao;
    private final ObjectMapper objectMapper;

    @Autowired
    public CommentService(KafkaTemplate<String, Comment> kafkaTemplate, CommentDao commentDao, ObjectMapper objectMapper) {
        this.kafkaTemplate = kafkaTemplate;
        this.commentDao = commentDao;
        this.objectMapper = objectMapper;
    }

    public Comment persist(String message) {
        return commentDao.save(new Comment(message));
    }

    public void publish(Comment comment) {
        ListenableFuture<SendResult<String, Comment>> future = kafkaTemplate.send(BvTopics.COMMENTS, comment);

        future.addCallback(new ListenableFutureCallback<>() {

            @Override
            public void onSuccess(SendResult<String, Comment> result) {
                try {
                    var msg = objectMapper.writeValueAsString(comment);
                    log.info(String.format("Message sent to broker: %s%n", msg));
                } catch (JsonProcessingException e) {
                    log.error(e.getMessage(), e);
                }
            }

            @Override
            public void onFailure(@NonNull Throwable ex) {
                log.error(String.format("Message with ID %d could not be sent", comment.id), ex);
            }
        });
    }
}
